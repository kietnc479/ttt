using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    public GameObject ChestPrefabs;

    Vector2 randPos;

    int randPosSpawn;
    void Start()
    {
        RandSpawnPos();
        SpawnRandom();
    }
    private void Awake()
    {
        randPosSpawn = Random.Range(1, 5);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void RandSpawnPos()
    {
        if (randPosSpawn == 1)
        {
            randPos = new Vector2(56.14f, 9.19f);
        }
        else if (randPosSpawn == 2)
        {
            randPos = new Vector2(49.5f, 9.31f);
        }
        else if (randPosSpawn == 3)
        {
            randPos = new Vector2(49.8f, 19.85f);
        }
        else if (randPosSpawn == 4)
        {
            randPos = new Vector2(54.18f, 19.85f);
        }
        else
        {
            randPos = new Vector2(64.71f, 17.9f);
        }
    }
    public void SpawnRandom()
    {
        Instantiate(ChestPrefabs, randPos, Quaternion.identity);
    }

}
